# Project Readme

## Running The Code Locally

This section **assumes you are running a Linux distrubution**, specifically Ubuntu. The code will probably run on something other than that, but no promises! Beyond that requirement, you also need to have basic familiarity with:

* [python 3.4](https://www.python.org/)
* [git](http://git-scm.com/)
* [pip](https://pip.pypa.io/en/latest/installing.html)
* [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
* [heroku toolbelt](https://toolbelt.heroku.com/)

**If you don't have any of those requirements**, you can install them via the commands below. If you are unsure if you have them, then go through the commands anyway!

    $ sudo add-apt-repository ppa:fkrull/deadsnakes
    $ sudo apt-get update
    $ sudo apt-get install python3.4 python3.4-dev
    $ sudo apt-get install git python-pip python-virtualenv heroku-toolbelt

**If this is your first time setting up Quirell**, you need to use these commands to set up the environment

    $ git clone git@gitlab.com:collectqt/quirell.git
    $ cd quirell
    $ virtualenv -p python3.4 venv

Finally, **everytime you start working on the project** you need to run these commands within the `quirell/` folder to activate the environment

    $ source venv/bin/activate

## Contributing

### Before You Make Changes

Figure out what sort of changes you are going to make, and how comfortable you are making them.

* If you are making a content (that is, not code) change you can do that via editing files on gitlab's online interface. For example [this link would allow you to edit the readme](https://gitlab.com/collectqt/quirell/edit/develop/readme.md)
* If you are a project maintainer and are adding large sections of code that nobody will disagree with, feel free to add them to branch **'develop'**
* If you are new to the project and want to make a suggested change (code or otherwise), create the feature on a new branch that describes the feature name (example: **'add-smiley-support'**) and submit a merge request versus **'develop'**

If the change you are going to make has [an issue](https://gitlab.com/collectqt/quirell/issues), make yourself the assignee for that issue once you know that you are going to start working on it.

### After You Make Changes

If you aren't a core maintainer, don't be concerned about making sure the documentation for your changes is perfect, or put extra effort into making sure your writing style matches the rest of the code - before submitting your changes. Feel free to **submit your changes after you have gone through the instructions listed below this paragraph**. If you are a core maintianer... make sure to add comments below you merge people's code !

If you added packages, add them to the requirements via

    $ pip freeze > requirements.txt

If you made any code changes, make sure the code runs successfully via running and stopping a debug server (so assets are rebuilt), then running and stopping a production sever (to see if anything breaks)

    $ python webapp/main.py
    $ foreman start

If you added a new folder, add it to the **Project Architecture** section of this readme

**If you are a core maintainer** you will be expected to pull changes from **'develop'** into **'production'**, then push them to heroku. The do that, run:

    $ git checkout production
    $ git merge develop
    $ git push heroku production:master

**But before you do this** make extra sure that the code is functional, because breaking production is not fun.

### Getting Developer Access

If you want developer access to the collectqt/quirell repository, ask a [CollectQT team member](https://gitlab.com/groups/collectqt/members) to give you developer access. At the current time, the specific team member you would ask for this is [Lynn Cyrin](https://gitlab.com/u/cyrin). Before asking for developer access, create or assign yourself to an issue to that other people know what you are working on.

## Project Architecture

    database/

remote database > local key value datastore functions

    webapp/

the website front end

    config/

Projectwide configuration and setup scripts
